#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

void Insertlist(int n, DLList *list)
{
	srand(time(NULL));
	int value;
	DLLElement *item;
	for (int i = 0; i < n; i++)
	{
		value = rand() % 100;
		item = new DLLElement(value);
		cout << "  key value: " << item->key;
		list->SortedInsert(item, value);
		cout << " item value: " << item->key << "  Sorted List: ";
		list->showlist();
		cout << endl;
	}
}

void Removelist(int n, DLList *list)
{
	for (int i = 0; i < n; i++) {
		int var;
		list->Remove(&var);
		cout << "Item " << var << " is removed" << endl;
		cout << "key: " << var;
		cout << "       Remain list: ";
		cout << "key: " << var << "         Sorted Remain List";
		list->showlist();
		cout << endl;
	}
}


